import A from '../components/test-a';
import B from '../components/test-b';
import C from '../components/test-c';

const routers = [
  {
    path: '/a',
    component: A,
  },
  {
    path: '/b',
    component: B,
  },
  {
    path: '/c',
    component: C,
  }
]

export default routers;