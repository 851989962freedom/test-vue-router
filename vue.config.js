module.exports = {
  lintOnSave: true,
  productionSourceMap: false,
  chainWebpack: (config) => {
    //忽略的打包文件
    config.externals({
      // 'vue': 'Vue',
      // 'vue-router': 'VueRouter',
      // 'vuex': 'Vuex',
      // 'axios': 'axios',
      // 'element-ui': 'ELEMENT',
    })
  },
  devServer: {
    // 端口配置
    port: 8081,
    disableHostCheck: true,
    // 反向代理配置
    proxy: {
      '/api': {
        target: "http://wms-api.bomman.com",
        ws: true,
        pathRewrite: {
          '^/api': '/'
        }
      }
    }
    // proxy: {
    //   '/api': {
    //     target: 'http://192.168.2.248:9000',
    //     ws: true,
    //     pathRewrite: {
    //       '^/api': '/'
    //     }
    //   }
    // }
  }
}
