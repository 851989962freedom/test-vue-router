import VueRouter from "vue-router";
import routers from "./pages";
import AvueRouter from './oksht-router';
import Vue from 'vue';

const Router = new  VueRouter({
  routes: [],
});

AvueRouter.install(Vue, Router);
routers.forEach((item) => Router.addRoute(item));
export default Router;