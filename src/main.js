import Vue from 'vue'
import VueRouter from "vue-router";
import App from './App.vue'
import Router from "./router/index";

Vue.config.productionTip = false;

Vue.use(VueRouter);
// Vue.use(window.AVUE);
console.log(Router);

new Vue({
  router: Router,
  render: h => h(App),
}).$mount('#app');

// a
