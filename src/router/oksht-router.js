let RouterPlugin = function () {
  this.$router = null;
};

RouterPlugin.install = function (vue, router) {
  this.$router = router;
  this.$vue = new vue();
  function isURL(s) {
    return /^http[s]?:\/\/.*/.test(s)
  }
  this.$router.$avueRouter = {
    //全局配置
    routerList: [],
    group: '',
    meta: {},
    safe: this,
    //动态路由
    formatRoutes: function (aMenu = [], first) {
      const aRouter = []
      const propsDefault = {
        label: 'name',
        path: 'path',
        icon: 'icon',
        children: 'children',
        meta: 'meta',
      }
      if (aMenu.length === 0) return;
      for (let i = 0; i < aMenu.length; i++) {
        const oMenu = aMenu[i];
        if (this.routerList.includes(oMenu[propsDefault.path])) return;
        const path = (() => {
            if (first) {
              return oMenu[propsDefault.path].replace('/index', '')
            } else {
              return oMenu[propsDefault.path]
            }
          })(),
          //特殊处理组件
          component = 'views' + oMenu.path,
          name = oMenu[propsDefault.label],
          icon = oMenu[propsDefault.icon],
          children = oMenu[propsDefault.children],
          meta = oMenu[propsDefault.meta] || {};

        const isChild = children.length !== 0;
        const oRouter = {
          path: path,
          component(resolve) {
            console.log(resolve);
            // 判断是否为首路由
            // if (first) {
            //   require(['../page/index'], resolve)
            //   return
            //   // 判断是否为多层路由
            // } else if (isChild && !first) {
            //   require(['../page/index/layout'], resolve)
            //   return
            //   // 判断是否为最终的页面视图
            // } else {
            //   require([`../${component}.vue`], resolve)
            // }
          },
          name: name,
          icon: icon,
          meta: meta,
          redirect: (() => {
            if (!isChild && first && !isURL(path)) return `${path}/index`
            else return '';
          })(),
          // 处理是否为一级路由
          children: !isChild ? (() => {
            if (first) {
              if (!isURL(path)) oMenu[propsDefault.path] = `${path}/index`;
              return [{
                component(resolve) { require([`../${component}.vue`], resolve) },
                icon: icon,
                name: name,
                meta: meta,
                path: 'index'
              }]
            }
            return [];
          })() : (() => {
            return this.formatRoutes(children, false)
          })()
        }
        aRouter.push(oRouter)
      }
      if (first) {
        if (!this.routerList.includes(aRouter[0][propsDefault.path])) {
          this.safe.$router.addRoutes(aRouter)
          this.routerList.push(aRouter[0][propsDefault.path])
        }
      } else {
        return aRouter
      }
    }
  }
}

export default RouterPlugin;
